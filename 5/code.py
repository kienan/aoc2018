#!/usr/bin/env python3

import string
import sys

def react(code, offset = 0):
    reacted = False
    current = None
    previous = None
    index = 0
    for c in code[offset:]:
        if index < 1:
            index += 1
            continue
        try:
            current = code[index]
        except IndexError:
            break
        previous = code[index-1]
        #print("Checking {} & {}".format(previous, current))
        if previous.upper() == current.upper():
            if previous != current:
                #print("{} and {} reacted around index {} ({})".format(previous, current, index, code[index-2:index+2]))
                reacted = True
                break
        index += 1
    if reacted:
        #start = index - 10
        #if start < 0:
        start = 0
        return (True, start, code[:index-1] + code[index+1:])
    else:
        return (False, index, code)

if __name__ == '__main__':
    lines = sys.stdin.readlines()
    # Part 1
    #for l in lines:
    #    reacted = True
    #    code = l.strip()
    #    index = 0
    #    iteration = 0
    #    while reacted:
    #        (reacted, index, code) = react(code, index)
    #        #print("{}, {}, iteration {}".format(reacted, index, iteration))
    #        iteration += 1
    #    print("Resulting code: '{}' ({} iterations) has {} units".format(code, iteration, len(code)))
    permutations = {}
    for l in lines:
        for character in string.ascii_lowercase:
            reacted = True
            code = l.strip()
            code = code.replace(character, '')
            code = code.replace(character.upper(), '')
            index = 0
            iteration = 0
            while reacted:
                (reacted, index, code) = react(code, index)
                iteration += 1
            permutations[character] = {
                'code': code,
                'size': len(code),
                'iterations': iteration,
            }
        for character in string.ascii_lowercase:
            print("Option {}{} result in code length {} ({} iterations)".format(
                character, character.upper(), permutations[character]['size'],
                permutations[character]['iterations']))
